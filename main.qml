import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480

    // простенький пример, как можно захуячить стиль
    // можно даже сделать несколько и менять их динамически,
    // ведь здесь кругом биндинги
    readonly property var itemStyle: ({
        borderColor: "gray",
        bgColor: "#10000000"
    })

    ListModel {
        id: todos

        function doneTodo(id, yn) { todos.setProperty(id, "done", yn); }
        function addTodo(text) { todos.append({ text: text, done: false }); }
        function removeTodo(id) { todos.remove(id); }
    }

    TextField {
        id: input

        function submit() {
            if (!text.length) return;
            todos.addTodo(text);
            text = "";
        }

        // растягиваем инпут слева до кнопки
        anchors {
            left: parent.left
            right: btnAdd.left
        }

        focus: true
        placeholderText: "Type TODO text here..."

        onAccepted: submit()
    }

    Button {
        id: btnAdd

        enabled: !!input.text.length
        // кнопка прибита с правой стороны
        anchors.right: parent.right
        text: "Add"

        onClicked: input.submit()
    }

    // Если потянуть за список или скролл -- то будет скролл =)))
    ListView {
        anchors {
            // прибит сверху к нижнему краю строки ввода
            top: input.bottom
            topMargin: 20
            // прибит снизу к нижнему краю окна
            bottom: parent.bottom
        }
        spacing: 5
        // список занимает всю ширину окна
        width: parent.width
        clip: true

        model: todos
        delegate: todoItem

        // захуячим анимашку добавления элемента
        add: Transition {
            OpacityAnimator { from: 0; to: 1.0; duration: 200 }
            NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 200 }
        }
        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.OutBounce }
        }
    }

    // Можно вынести в отдельный файл
    Component {
        id: todoItem

        // Подложка в виде прямоугольного блока
        Rectangle {
            border.color: itemStyle.borderColor
            color: itemStyle.bgColor

            height: childrenRect.height
            width: parent.width

            // Горизонтальный Layout: текст + кнопка
            // (размещает внутри себя элементы сам)
            RowLayout {
                width: parent.width

                Text {
                    // Растягиваем текст по-максимуму
                    Layout.fillWidth: true
                    Layout.margins: 5

                    text: model.text
                    elide: Text.ElideRight
                    // биндинг к property модели текущего элемента
                    font.strikeout: !!model.done

                    MouseArea {
                        // Занимает всю область родителя
                        anchors.fill: parent
                        onClicked: {
                            // меняем значение в модели списка элементов
                            // (если написать модель на C++, то этот кусок мог бы
                            // вообще выглядеть так:
                            // model.done = !model.done
                            todos.doneTodo(model.index, !model.done);
                        }
                    }
                }

                Button {
                    text: "Delete"
                    onClicked: todos.removeTodo(model.index)
                }
            }
        }
    }
}

