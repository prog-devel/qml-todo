REQUIREMENTS
============

* Straight arms
* Qt >= 5.2

BUILDING / RUN
==============

qmake && make -j8 && ./out/qml-todo
